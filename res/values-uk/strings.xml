<?xml version="1.0" encoding="utf-8"?>
<!--
     Copyright (C) 2017 The LineageOS Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    <!-- App name -->
    <string name="app_name">Оновлення системи</string>
    <!-- Notifications -->
    <string name="export_channel_title">Експорт завершено</string>
    <string name="new_updates_channel_title">Нові оновлення</string>
    <string name="ongoing_channel_title">Поточні завантаження</string>
    <string name="update_failed_channel_title">Оновлення провалено</string>
    <string name="verification_failed_notification">Не вдалося виконати перевірку</string>
    <string name="verifying_download_notification">Перевірка оновлення</string>
    <string name="downloading_notification">Завантаження</string>
    <string name="download_paused_notification">Завантаження призупинено</string>
    <string name="download_paused_error_notification">Помилка завантаження</string>
    <string name="download_completed_notification">Завантаження успішне</string>
    <string name="download_starting_notification">Початок завантаження</string>
    <string name="update_failed_notification">Оновлення провалено</string>
    <string name="installation_suspended_notification">Встановлення призупинено</string>
    <!-- Menu -->
    <string name="menu_refresh">Оновити</string>
    <string name="menu_preferences">Налаштування</string>
    <string name="menu_auto_updates_check">Автоматична перевірка оновлень</string>
    <string name="menu_auto_updates_check_interval_daily">Раз на день</string>
    <string name="menu_auto_updates_check_interval_weekly">Раз на тиждень</string>
    <string name="menu_auto_updates_check_interval_monthly">Раз на місяць</string>
    <string name="menu_auto_updates_check_interval_never">Ніколи</string>
    <string name="menu_delete_update">Видалити</string>
    <string name="menu_copy_url">Копіювати адресу</string>
    <string name="menu_export_update">Експорт оновлення</string>
    <string name="menu_show_changelog">Зміни поточної версії</string>
    <string name="changelog_loading">Завантаження...</string>
    <string name="label_download_url">Адреса завантаження</string>
    <string name="toast_download_url_copied">Адресу скопійовано</string>
    <!-- General -->
    <string name="update_found_notification">Доступне оновлення</string>
    <string name="snack_no_updates_found">Оновлень не знайдено</string>
    <string name="snack_updates_check_failed">Не вдалося перевірити оновлення. Будь-ласка, перевірте підключення до Інтернету і повторіть спробу пізніше.</string>
    <string name="snack_download_failed">Завантаження оновлення не вдалось. Будь-ласка перевірте з’єднання з мережею і спробуйте ще раз.</string>
    <string name="snack_download_verification_failed">Помилка перевірки оновлення.</string>
    <string name="snack_download_verified">Завантаження успішне.</string>
    <string name="snack_update_not_installable">Це оновлення не можна інсталювати на поточну збірку.</string>
    <string name="text_download_speed">%1$s, %2$s/с</string>
    <string name="list_download_progress_new"><xliff:g example="12.2" id="filesize_without_unit">%1$s</xliff:g> of <xliff:g example="310 MB" id="filesize_without_unit">%2$s</xliff:g> • <xliff:g example="56" id="percentage">%3$s</xliff:g></string>
    <string name="list_download_progress_eta_new"><xliff:g example="12.2" id="filesize_without_unit">%1$s</xliff:g> of <xliff:g example="310 MB" id="filesize_without_unit">%2$s</xliff:g> (<xliff:g example="3 минуты осталось" id="eta">%3$s</xliff:g>) • <xliff:g example="56" id="percentage">%4$s</xliff:g></string>
    <string name="list_verifying_update">Перевірка оновлення</string>
    <string name="list_no_updates">Оновлень не знайдено. Щоб вручну перевірити наявність оновлень, натисніть кнопку Оновити.</string>
    <string name="blocked_update_dialog_title">Оновлення заблоковано</string>
    <!-- Actions -->
    <string name="action_download">Завантажити</string>
    <string name="action_pause">Призупинити</string>
    <string name="action_resume">Продовжити</string>
    <string name="action_install">Встановити</string>
    <string name="action_delete">Видалити</string>
    <string name="action_cancel">Скасувати</string>
    <string name="confirm_delete_dialog_title">Видалити файл</string>
    <string name="confirm_delete_dialog_message">Видалити обраний файл оновлення?</string>
    <string name="pause_button">Пауза</string>
    <string name="resume_button">Продовжити</string>
    <string name="suspend_button">Призупинити</string>
    <string name="reboot">Перезавантаження</string>
    <string name="details_button">Список змін</string>
    <string name="changelog_fail">Не вдалося отримати список змін</string>
    <!-- Installation -->
    <string name="apply_update_dialog_title">Застосувати оновлення</string>
    <string name="apply_update_dialog_message">Ви збираєтесь оновитися до <xliff:g id="update_name">%1$s</xliff:g>.\n\nЯкщо ви натиснете <xliff:g id="ok">%2$s</xliff:g>, пристрій перезавантажиться в режим відновлення для встановлення оновлення.\n\nПримітка. Ця функція потребує сумісного відновлення або оновлення для цього повинна бути встановлена вручну.</string>
    <string name="apply_update_dialog_message_ab">Ви збираєтесь оновитись на <xliff:g id="update_name">%1$s</xliff:g>.\n\nЯкщо ви натиснети на <xliff:g id="ok">%2$s</xliff:g>, пристрій почне встановлення у фоновому режимі.\n\nПісля встановлення вам буде запропоновано перезавантажити пристрій.</string>
    <string name="cancel_installation_dialog_message">Скасувати встановлення?</string>
    <string name="installing_update">Інсталяція пакету оновлення</string>
    <string name="installing_update_error">Помилка встановлення</string>
    <string name="installing_update_finished">Оновлення встановлено</string>
    <string name="finalizing_package">Завершення встановлення</string>
    <string name="preparing_ota_first_boot">Підготовка до першого старту</string>
    <string name="dialog_prepare_zip_message">Підготовка оновлення</string>
    <string name="dialog_battery_low_title">Низький заряд батареї</string>
    <string name="dialog_battery_low_message_pct">Рівень заряду батареї занизький, для продовження необхідно принаймні <xliff:g id="percent_discharging">%1$d</xliff:g>%% заряду або <xliff:g id="percent_charging">%2$d</xliff:g>%% під час зарядки.</string>
    <!-- Export update -->
    <string name="dialog_export_title">Експорт оновлення</string>
    <string name="notification_export_success">Оновлення експортовано</string>
    <string name="notification_export_fail">Помилка експорту</string>
    <string name="toast_already_exporting">Оновлення вже експортується</string>
    <!-- ETA -->
    <plurals name="eta_seconds">
        <item quantity="one">Залишилась одна секунда</item>
        <item quantity="few">залишилось <xliff:g id="count">%d</xliff:g> секунди</item>
        <item quantity="many">залишилось <xliff:g id="count">%d</xliff:g> секунди</item>
        <item quantity="other">залишилось <xliff:g id="count">%d</xliff:g> секунди</item>
    </plurals>
    <plurals name="eta_minutes">
        <item quantity="one">Залишилась одна хвилина</item>
        <item quantity="few">залишилось <xliff:g id="count">%d</xliff:g> хвилини</item>
        <item quantity="many">залишилось <xliff:g id="count">%d</xliff:g> хвилини</item>
        <item quantity="other">залишилось <xliff:g id="count">%d</xliff:g> хвилини</item>
    </plurals>
    <plurals name="eta_hours">
        <item quantity="one">Залишилась одна година</item>
        <item quantity="few">Залишилось <xliff:g id="count">%d</xliff:g> годин</item>
        <item quantity="many">Залишилось <xliff:g id="count">%d</xliff:g> годин</item>
        <item quantity="other">Залишилось <xliff:g id="count">%d</xliff:g> годин</item>
    </plurals>
    <!-- Mobile data warning -->
    <string name="update_on_mobile_data_title">Попередження</string>
    <string name="update_on_mobile_data_message">Ви збираєтеся завантажити пакет оновлення за допомогою мобільної мережі, що, ймовірно, призведе до високого використання даних. Ви хочете продовжити?</string>
    <string name="checkbox_mobile_data_warning">Не показувати знову</string>
    <string name="menu_mobile_data_warning">Попередження про мобільні дані</string>
    <!-- Info section -->
    <string name="extras_category_title">Додатки</string>
    <string name="developer_info_title">Головний розробник</string>
    <string name="maintainer_info_title">Супроводжуючі пристрої</string>
    <string name="donate_info_title">Пожертвувати розробнику</string>
    <string name="donate_info_summary">Підтримка розвитку</string>
    <string name="support_info_title">Пожертвуйте на технічне обслуговування пристрою</string>
    <string name="support_info_summary">Підтримка технічного обслуговування пристрою</string>
    <string name="website_info_title">Вебсайт</string>
    <string name="website_info_summary">Відвідати сайт</string>
    <string name="news_info_title">Новини</string>
    <string name="news_info_summary">Стежте за оновленнями та іншими новинами</string>
    <string name="forum_info_title">Нова гілка</string>
    <string name="forum_info_summary">Останні дописи форуму</string>
    <string name="error_open_url">Не вдалося відкрити посилання у браузері</string>
</resources>
